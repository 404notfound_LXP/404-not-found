/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

import Business.Airline.Airplane;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author xieli
 */
public class AirlinerDirectory {
    public ArrayList<Airliner> airlinerDir;

    
    public AirlinerDirectory() throws ParseException{
        airlinerDir = new ArrayList<Airliner>();
//        try {
//            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-30");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        try {
//          Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-31");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        try {
//          Date date3 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-11-01");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

//        Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-31");
//        Date date3 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-11-01");
        
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//这个是你要转成后的时间的格式
//        
//        Date date1 = new Date(Long.parseLong("20191030"));
//        Date date2 = new Date(Long.parseLong("20191031"));
//        Date date3 = new Date(Long.parseLong("20191101"));

          DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
          Date date1 = dateFormat1.parse("2019-10-30");
          Date date2 = dateFormat1.parse("2019-10-31");
          Date date3 = dateFormat1.parse("2019-11-01");

        
        FlightDirectory flightDir1 = new FlightDirectory();
        FlightDirectory flightDir2 = new FlightDirectory();
        FlightDirectory flightDir3 = new FlightDirectory();
        
        AirplaneDirectory airplaneDir1 = new AirplaneDirectory();
        AirplaneDirectory airplaneDir2 = new AirplaneDirectory();
        AirplaneDirectory airplaneDir3 = new AirplaneDirectory();
        
        //Flight(String model, String airliner, String flightNum, String departure, String destination, String scheduledTime, String arrivalTime, int Price, int seatNum, String status, Date date)
        
        Flight flightA1 = new Flight("Boeing 737", "AirAsia", "CZ6903", "Boston", "Beijing", "Morning", "Morning", 1507, 150, "Normal", date1);
        Flight flightA2 = new Flight("Boeing 737", "AirAsia", "CZ6903", "Boston", "Beijing", "Morning", "Afternoon", 1430, 150, "Normal", date2);
        Flight flightA3 = new Flight("Boeing 747", "AirAsia", "AA2503", "Boston", "Chicago", "Evening", "Morning", 485, 150, "Normal", date1);
        Flight flightA4 = new Flight("Boeing 747", "AirAsia", "AA2503", "Boston", "Chicago", "Evening", "Morning", 621, 150, "Normal", date2);
        Flight flightA5 = new Flight("Boeing 747", "AirAsia", "AA2503", "Boston", "Chicago", "Afternoon", "Morning", 421, 150, "Normal", date3);
        Flight flightA6 = new Flight("Airbus A350", "AirAsia", "AA6483", "New York", "Shanghai", "Evening", "Afternoon", 1920, 150, "Normal", date1);
        Flight flightA7 = new Flight("Airbus A350", "AirAsia", "AA6483", "New York", "Shanghai", "Morning", "Afternoon", 1840, 150, "Normal", date3);
        Airplane airplane1 = new Airplane("Boeing 737", "AirAsia", flightA1);
        Airplane airplane2 = new Airplane("Boeing 737", "AirAsia", flightA2);
        Airplane airplane3 = new Airplane("Boeing 747", "AirAsia", flightA3);
        Airplane airplane4 = new Airplane("Boeing 747", "AirAsia", flightA4);
        Airplane airplane5 = new Airplane("Boeing 747", "AirAsia", flightA5);
        Airplane airplane6 = new Airplane("Airbus A350", "AirAsia", flightA6);
        Airplane airplane7 = new Airplane("Airbus A350", "AirAsia", flightA7);
        
        flightA1.setAirplane(airplane1);
        flightA2.setAirplane(airplane2);
        flightA3.setAirplane(airplane3);
        flightA4.setAirplane(airplane4);
        flightA5.setAirplane(airplane5);
        flightA6.setAirplane(airplane6);
        flightA7.setAirplane(airplane7);
        
        flightDir1.addFlight(flightA1);
        flightDir1.addFlight(flightA2);
        flightDir1.addFlight(flightA3);
        flightDir1.addFlight(flightA4);
        flightDir1.addFlight(flightA5);
        flightDir1.addFlight(flightA6);
        flightDir1.addFlight(flightA7);
        airplaneDir1.addAirplane(airplane1);
        airplaneDir1.addAirplane(airplane2);
        airplaneDir1.addAirplane(airplane3);
        airplaneDir1.addAirplane(airplane4);
        airplaneDir1.addAirplane(airplane5);
        airplaneDir1.addAirplane(airplane6);
        airplaneDir1.addAirplane(airplane7);
        for (int i=0; i<4; i++){
            airplaneDir1.addAirplane(new Airplane("Boeing 737", "AirAsia", null));
            airplaneDir1.addAirplane(new Airplane("Boeing 747", "AirAsia", null));
            airplaneDir1.addAirplane(new Airplane("Airbus A350", "AirAsia", null));
        }

        
        Flight flightB1 = new Flight("Airbus A300", "Delta", "DD2035", "Boston", "Beijing", "Evening", "Afternoon", 1201, 150, "Normal", date1);
        Flight flightB2 = new Flight("Airbus A300", "Delta", "DD2035", "Boston", "Beijing", "Morning", "Afternoon", 1325, 150, "Normal", date2);
        Flight flightB3 = new Flight("Airbus A350", "Delta", "DD2035", "Boston", "Beijing", "Afternoon", "Evening", 1120, 150, "Normal", date3);
        Flight flightB4 = new Flight("Boeing 377", "Delta", "DD5483", "Chicago", "New York", "Morning", "Morning", 321, 150, "Normal", date1);
        Flight flightB5 = new Flight("Boeing 377", "Delta", "DD5483", "Chicago", "New York", "Evening", "Morning", 215, 150, "Normal", date2);
        Flight flightB6 = new Flight("Boeing 377", "Delta", "DD5483", "Chicago", "New York", "Afternnon", "Afternoon", 215, 150, "Normal", date3);
        Flight flightB7 = new Flight("Boeing 777", "Delta", "DD5483", "Chicago", "New York", "Morning", "Afternoon", 436, 150, "Normal", date2);
        Flight flightB8 = new Flight("Boeing 777", "Delta", "DD4132", "Boston", "New York", "Morning", "Afternoon", 321, 150, "Normal", date1);
        Flight flightB9 = new Flight("Boeing 747", "Delta", "DD4132", "Boston", "New York", "Evening", "Evening", 189, 150, "Normal", date1);
        Flight flightB10 = new Flight("Boeing 747", "Delta", "DD4132", "Boston", "New York", "Afternnon", "Evening", 234, 150, "Normal", date2);
        
        Airplane airplane11 = new Airplane("Airbus A300", "Delta", flightB1);
        Airplane airplane12 = new Airplane("Airbus A300", "Delta", flightB2);
        Airplane airplane13 = new Airplane("Airbus A350", "Delta", flightB3);
        Airplane airplane14 = new Airplane("Boeing 747", "Delta", flightB4);
        Airplane airplane15 = new Airplane("Boeing 747", "Delta", flightB5);
        Airplane airplane16 = new Airplane("Boeing 377", "Delta", flightB6);
        Airplane airplane17 = new Airplane("Boeing 777", "Delta", flightB7);
        Airplane airplane18 = new Airplane("Boeing 777", "Delta", flightB8);
        Airplane airplane19 = new Airplane("Boeing 747", "Delta", flightB9);
        Airplane airplane20 = new Airplane("Boeing 747", "Delta", flightB10);
        
        flightB1.setAirplane(airplane11);
        flightB2.setAirplane(airplane12);
        flightB3.setAirplane(airplane13);
        flightB4.setAirplane(airplane14);
        flightB5.setAirplane(airplane15);
        flightB6.setAirplane(airplane16);
        flightB7.setAirplane(airplane17);
        flightB8.setAirplane(airplane18);
        flightB9.setAirplane(airplane19);
        flightB10.setAirplane(airplane20);

        
        flightDir2.addFlight(flightB1);
        flightDir2.addFlight(flightB2);
        flightDir2.addFlight(flightB3);
        flightDir2.addFlight(flightB4);
        flightDir2.addFlight(flightB5);
        flightDir2.addFlight(flightB6);
        flightDir2.addFlight(flightB7);
        flightDir2.addFlight(flightB8);
        flightDir2.addFlight(flightB9);
        flightDir2.addFlight(flightB10);
        airplaneDir2.addAirplane(airplane11);
        airplaneDir2.addAirplane(airplane12);
        airplaneDir2.addAirplane(airplane13);
        airplaneDir2.addAirplane(airplane14);
        airplaneDir2.addAirplane(airplane15);
        airplaneDir2.addAirplane(airplane16);
        airplaneDir2.addAirplane(airplane17);
        airplaneDir2.addAirplane(airplane18);
        airplaneDir2.addAirplane(airplane19);
        airplaneDir2.addAirplane(airplane20);
        for (int i=0; i<3; i++){
            airplaneDir2.addAirplane(new Airplane("Airbus A350", "Delta", null));
            airplaneDir2.addAirplane(new Airplane("Airbus A300", "Delta", null));
            airplaneDir2.addAirplane(new Airplane("Boeing 747", "Delta", null));
            airplaneDir2.addAirplane(new Airplane("Boeing 377", "Delta", null));
            airplaneDir2.addAirplane(new Airplane("Boeing 777", "Delta", null));
        }
        
        Flight flightC1 = new Flight("Airbus A350", "American", "AN2035", "Boston", "Beijing", "Evening", "Afternoon", 1657, 150, "Normal", date1);
        Flight flightC2 = new Flight("Airbus A300", "American", "AN2035", "Boston", "Beijing", "Afternoon", "Evening", 1782, 150, "Normal", date2);
        Flight flightC3 = new Flight("Airbus A350", "American", "AN2035", "Boston", "Beijing", "Morning", "Afternoon", 1865, 150, "Normal", date3);
        Flight flightC4 = new Flight("Boeing 307", "American", "AN5483", "New York", "Beijing", "Morning", "Morning", 1880, 150, "Normal", date1);
        Flight flightC5 = new Flight("Boeing 307", "American", "AN5483", "New York", "Beijing", "Evening", "Morning", 1932, 150, "Normal", date2);
        Flight flightC6 = new Flight("Boeing 307", "American", "AN5483", "New York", "Beijing", "Afternnon", "Afternoon", 1743, 150, "Normal", date3);
        Flight flightC7 = new Flight("Boeing 777", "American", "AN5433", "Chicago", "Boston", "Morning", "Evening", 312, 150, "Normal", date2);
        Flight flightC8 = new Flight("Boeing 777", "American", "AN5443", "Chicago", "Boston", "Morning", "Afternoon", 243, 150, "Normal", date1);
        Flight flightC9 = new Flight("Boeing 747", "American", "AN5435", "Chicago", "Boston", "Evening", "Evening", 156, 150, "Normal", date3);
        
        Airplane airplane21 = new Airplane("Airbus A350", "American", flightC1);
        Airplane airplane22 = new Airplane("Airbus A300", "American", flightC2);
        Airplane airplane23 = new Airplane("Airbus A350", "American", flightC3);
        Airplane airplane24 = new Airplane("Boeing 307", "American", flightC4);
        Airplane airplane25 = new Airplane("Boeing 307", "American", flightC5);
        Airplane airplane26 = new Airplane("Boeing 307", "American", flightC6);
        Airplane airplane27 = new Airplane("Boeing 777", "American", flightC7);
        Airplane airplane28 = new Airplane("Boeing 777", "American", flightC8);
        Airplane airplane29 = new Airplane("Boeing 747", "American", flightC9);
        
        flightC1.setAirplane(airplane21);
        flightC2.setAirplane(airplane22);
        flightC3.setAirplane(airplane23);
        flightC4.setAirplane(airplane24);
        flightC5.setAirplane(airplane25);
        flightC6.setAirplane(airplane26);
        flightC7.setAirplane(airplane27);
        flightC8.setAirplane(airplane28);
        flightC9.setAirplane(airplane29);
        
        flightDir3.addFlight(flightC1);
        flightDir3.addFlight(flightC2);
        flightDir3.addFlight(flightC3); 
        flightDir3.addFlight(flightC4);
        flightDir3.addFlight(flightC5);
        flightDir3.addFlight(flightC6);
        flightDir3.addFlight(flightC7);
        flightDir3.addFlight(flightC8);
        flightDir3.addFlight(flightC9);
        airplaneDir3.addAirplane(airplane21);
        airplaneDir3.addAirplane(airplane22);
        airplaneDir3.addAirplane(airplane23);
        airplaneDir3.addAirplane(airplane24);
        airplaneDir3.addAirplane(airplane25);
        airplaneDir3.addAirplane(airplane26);
        airplaneDir3.addAirplane(airplane27);
        airplaneDir3.addAirplane(airplane28);
        airplaneDir3.addAirplane(airplane29);
        for (int i=0; i<4; i++){
            airplaneDir3.addAirplane(new Airplane("Airbus A350", "Delta", null));
            airplaneDir3.addAirplane(new Airplane("Airbus A300", "Delta", null));
            airplaneDir3.addAirplane(new Airplane("Boeing 747", "Delta", null));
            airplaneDir3.addAirplane(new Airplane("Boeing 307", "Delta", null));
            airplaneDir3.addAirplane(new Airplane("Boeing 777", "Delta", null));
        }

        
        Airliner airliner1 = new Airliner("AirAsia", airplaneDir1, flightDir1);
        Airliner airliner2 = new Airliner("Delta", airplaneDir2, flightDir2);
        Airliner airliner3 = new Airliner("American", airplaneDir3, flightDir3);
        
        airlinerDir.add(airliner1);
        airlinerDir.add(airliner2);
        airlinerDir.add(airliner3);
      
    }
    
    public ArrayList<Airliner> getAirlinerDirectory(){
        return airlinerDir;
    }
    
    public void setAirlinerDirectory(ArrayList<Airliner> airlinerDir){
        this.airlinerDir = airlinerDir;
    }
    
    public Airliner addAirliner(){
        Airliner airliner = new Airliner();
        airlinerDir.add(airliner);
        return airliner;
    }
    
    public void deleteAirliner(Airliner airliner){
        airlinerDir.remove(airliner);
    }
}
