/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

/**
 *
 * @author xieli
 */
public class Airplane {
    private int number;
    private String model;
    private String airliner;
    private Flight flight;
    
    public Airplane(String model, String airliner, Flight flight){
        this.model = model;
        this.airliner = airliner;
        this.flight = flight;
    }
    
    public Airplane(){
        
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    public Flight getFlight() {
        return flight;
    }
    
    public String getFlightInfo(){
        String information = flight.getDeparture() + " - " + flight.getDestination() + ", " + flight.getScheduledTime() + " - " + flight.getArrivalTime() + " " + flight.getDate();
        return (information);
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
    
    
    
    @Override
    public String toString() {
        return String.valueOf(getNumber());
    }
    
}
