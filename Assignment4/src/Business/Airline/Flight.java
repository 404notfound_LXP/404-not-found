/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author xieli
 */
public class Flight {
    private String model;
    private String airliner;
    private String flightNum;
    private String departure;
    private String destination;
    private String scheduledTime;
    private String arrivalTime;
    private int Price;
    private int seatNum;
    private String status;
    private Date date = new Date();
    private Airplane airplane;
    private String[] array1;
    private String[] array2;
    private String[] array3;
    private String[] array4;
    private String[] array5;
    private String[] array6;
    public ArrayList<String> seatList1;
    public ArrayList<String> seatList2;
    public ArrayList<String> seatList3;
    public ArrayList<String> seatList4;
    public ArrayList<String> seatList5;
    public ArrayList<String> seatList6;

    
    public Flight(String model, String airliner, String flightNum, String departure, String destination, String scheduledTime, String arrivalTime, int Price, int seatNum, String status, Date date){
        this.model = model;
        this.airliner = airliner;
        this.flightNum = flightNum;
        this.departure = departure;
        this.destination = destination;
        this.scheduledTime = scheduledTime;
        this.arrivalTime = arrivalTime;
        this.Price = Price;
        this.seatNum = seatNum;
        this.status = status;
        this.date = date;
        initialize();
    }
    private void initialize(){
        seatList1 = new ArrayList<>();
        seatList2 = new ArrayList<>();
        seatList3 = new ArrayList<>();
        seatList4 = new ArrayList<>();
        seatList5 = new ArrayList<>();
        seatList6 = new ArrayList<>();
        array1 = new String[]{"A0","A1","A2","A3","A4","A5","A6","A7","A8","A9","A10","A11","A12","A13","A14","A15","A16","A17","A18","A19","A20","A21","A22","A23","A24"};
        array2 = new String[]{"A0","A1","A2","A3","A4","A5","A6","A7","A8","A9","A10","A11","A12","A13","A14","A15","A16","A17","A18","A19","A20","A21","A22","A23","A24"};
        array3 = new String[]{"A0","A1","A2","A3","A4","A5","A6","A7","A8","A9","A10","A11","A12","A13","A14","A15","A16","A17","A18","A19","A20","A21","A22","A23","A24"};
        array4 = new String[]{"B0","B1","B2","B3","B4","B5","B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","B17","B18","B19","B20","B21","B22","B23","B24"};
        array5 = new String[]{"B0","B1","B2","B3","B4","B5","B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","B17","B18","B19","B20","B21","B22","B23","B24"};
        array6 = new String[]{"B0","B1","B2","B3","B4","B5","B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","B17","B18","B19","B20","B21","B22","B23","B24"};
        for(String s:array1){
            seatList1.add(s);
        }
        for(String s:array2){
            seatList2.add(s);
        }
        for(String s:array3){
            seatList3.add(s);
        }
        for(String s:array4){
            seatList4.add(s);
        }
        for(String s:array5){
            seatList5.add(s);
        }
        for(String s:array6){
            seatList6.add(s);
        }
        
    }
    
    public Flight(){
        
    }

    public String getModel() {
        return model;
    }

    public void setModel(String name) {
        this.model = model;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    public String getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(String scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }
    

    
    @Override
    public String toString() {
        return airplane.getModel();
    }
    
}
