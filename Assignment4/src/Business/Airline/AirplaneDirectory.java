/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

import Business.Airline.Airplane;
import java.util.ArrayList;

/**
 *
 * @author xieli
 */
public class AirplaneDirectory {
    public ArrayList<Airplane> airplaneDir;
    
    
    
    
    public AirplaneDirectory(){
        airplaneDir = new ArrayList<Airplane>();
    }
    
    public int getAirplaneDirectorySize(){
        return airplaneDir.size();
    }
    
     public ArrayList<Airplane> getAirplaneDirectory(){
        return airplaneDir;
    }
    
    public void setAirplaneDirectory(ArrayList<Airplane> airplaneDir){
        this.airplaneDir = airplaneDir;
    } 
    
    public Airplane addAirplane(){
        Airplane airplane = new Airplane();
        airplaneDir.add(airplane);
        return airplane;
    }
    
    public Airplane addAirplane(Airplane airplane){
        airplaneDir.add(airplane);
        return airplane;
    }
    
 
    
    public void deleteAirplane(Airplane airplane){
        airplaneDir.remove(airplane);
    }
}
