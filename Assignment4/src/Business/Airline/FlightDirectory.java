/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author xieli
 */
public class FlightDirectory {
    public ArrayList<Flight> flightList;
    public ArrayList<Flight> flightResultDir;
    
    public FlightDirectory(){
        flightList = new ArrayList<Flight>();
        flightResultDir = new ArrayList<Flight>();
      
    }
    
    public int getFlightDirectorySize(){
        return flightList.size();
    }
    
    public ArrayList<Flight> getFlightDirectory(){
        return flightList;
    }
    
    public void setFlightDirectory(ArrayList<Flight> flightDir){
        this.flightList = flightDir;
    } 
    
    public Flight addFlight(){
        Flight flight = new Flight();
        flightList.add(flight);
        return flight;
    }
    
    public Flight addFlight(Flight flight){
        flightList.add(flight);
        return flight;
    }
    
    public void deleteFlight(Flight flight){
        flightList.remove(flight);
    }
    
     public ArrayList<Flight> getSearchList(String year, String month, String day){
        flightResultDir.clear();
        
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String date = year+"-"+month+"-"+day;
        for(Flight flight:flightList){
            if(flight.getDate().equals(date)){
                flightResultDir.add(flight);
            }
        }

        return flightResultDir;
    }
}
