/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Airline;

/**
 *
 * @author xieli
 */
public class Airliner {
    private String name;
    
    private int airplaneNum;
    private FlightDirectory flightDir;
    private int routeNum;
    private AirplaneDirectory airplaneDir;
    
    public Airliner(String name, AirplaneDirectory airplaneDir, FlightDirectory flightDir){
        this.name = name;
        this.flightDir = flightDir;
        this.airplaneDir = airplaneDir;
    }
    
    
    public Airliner() {
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRouteNum() {
        return routeNum;
    }

    public void setRouteNum(int routeNum) {
        this.routeNum = routeNum;
    }

    public int getAirplaneNum() {
        return airplaneNum;
    }

    public void setAirplaneNum(int airplaneNum) {
        this.airplaneNum = airplaneNum;
    }

    public FlightDirectory getFlightDir() {
        return flightDir;
    }

    public void setFlightDir(FlightDirectory flightDir) {
        this.flightDir = flightDir;
    }

    public AirplaneDirectory getAirplaneDir() {
        return airplaneDir;
    }

    public void setAirplaneDir(AirplaneDirectory airplaneDir) {
        this.airplaneDir = airplaneDir;
    }
    
    
    
    
    @Override
    public String toString() {
        return getName();
    }

 
    
    
    
}

