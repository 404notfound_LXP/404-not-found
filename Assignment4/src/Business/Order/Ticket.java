/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Order;

import Business.Airline.Flight;
import Business.People.Customer;
import java.util.Random;

/**
 *
 * @author apple
 */
public class Ticket {

    private Flight flight;
    private Customer customer;
    //TODO 初始化
    private String OrderName;
    private boolean OrderStatus = true;
    private String seat;

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public String getOrderName() {
        return OrderName;
    }

    public void setOrderName(String OrderName) {
        this.OrderName = OrderName;
    }

    public boolean isOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(boolean OrderStatus) {
        this.OrderStatus = OrderStatus;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public Ticket(Flight flight, Customer customer,String seat) {
        this.flight = flight;
        this.customer = customer;
        this.seat = seat;
        this.OrderName = new Random().nextInt(9999)+"";
    }

    public Flight getFleet() {
        return flight;
    }

    public void setFleet(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return OrderName;
    }
    
    
}
