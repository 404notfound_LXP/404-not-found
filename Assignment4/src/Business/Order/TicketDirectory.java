/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Order;

import Business.Airline.Flight;
import Business.People.Customer;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author apple
 */
public class TicketDirectory {
    private ArrayList<Ticket> ticketList;
    private Ticket ticket;

    public TicketDirectory() {
        ticketList = new ArrayList<Ticket>();
        
        //test case+++++++
        //Date date = new Date();
        //Flight flight = new Flight("","","","","","","",1,1,"",date);
        //Customer customer = new Customer();
        //customer.setName("haha");
        //customer.setUsername("hahaha");
        //customer.setOffice("1");
        //ticket = new Ticket(flight,customer,"A1_Window");
        //ticketList.add(ticket);
    }

    public ArrayList<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(ArrayList<Ticket> ticketList) {
        this.ticketList = ticketList;
    }
}
