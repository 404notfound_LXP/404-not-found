/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.People;
import java.util.Date;

/**
 *
 * @author pmh
 */
public class Customer {
    
    private String name;
    private String office;
    private String username;
    private Date createdOn;
    
    public Customer(String name,String office,String username){
        this.createdOn = new Date();
        this.name = name;
        this.office = office;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public Date getCreatedOn() {
        return createdOn;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
}
