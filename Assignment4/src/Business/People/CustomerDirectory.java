/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.People;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pmh
 */
public class CustomerDirectory {
    
   private List<Customer> customerList;
    
    public CustomerDirectory(){
        customerList = new ArrayList<>();
        customerList.add(new Customer("lzh","MA","lzh"));
        customerList.add(new Customer("xlj","MA","xlj"));
        customerList.add(new Customer("psz","MA","psz"));
        
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public void addCustomer(Customer c){
        customerList.add(c);
    }
    public void deleteCustomer(Customer c){
        customerList.remove(c);
    }
    
}    

