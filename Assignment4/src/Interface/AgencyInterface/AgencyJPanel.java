/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.AgencyInterface;

import Business.People.CustomerDirectory;
import Business.Airline.FlightDirectory;
import Business.Order.TicketDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author xieli
 */
public class AgencyJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private CustomerDirectory customerDirectory;
    private FlightDirectory flightDirectory;
    private TicketDirectory ticketDirectory;

    public AgencyJPanel(JPanel panelRight, CustomerDirectory customerDirectory, FlightDirectory flightDirectory, TicketDirectory ticketDirectory) {
        initComponents();
        this.userProcessContainer = panelRight;
        this.customerDirectory = customerDirectory;
        this.flightDirectory = flightDirectory;
        this.ticketDirectory = ticketDirectory;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backBtn = new javax.swing.JButton();
        mngCtmBtn = new javax.swing.JButton();
        bookBtn = new javax.swing.JButton();
        btnOrderHistory = new javax.swing.JButton();

        backBtn.setText("< Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        mngCtmBtn.setText("Manage customer information");
        mngCtmBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mngCtmBtnActionPerformed(evt);
            }
        });

        bookBtn.setText("Book tickets");
        bookBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookBtnActionPerformed(evt);
            }
        });

        btnOrderHistory.setText("Order history");
        btnOrderHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderHistoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(211, 211, 211)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mngCtmBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 267, Short.MAX_VALUE)
                            .addComponent(bookBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnOrderHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(426, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(backBtn)
                .addGap(51, 51, 51)
                .addComponent(mngCtmBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(bookBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(btnOrderHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void mngCtmBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mngCtmBtnActionPerformed
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add(new MngCustomerJPanel(userProcessContainer,customerDirectory));
        layout.next(userProcessContainer);        
    }//GEN-LAST:event_mngCtmBtnActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        userProcessContainer.remove(this);            
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed

    private void bookBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookBtnActionPerformed
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add(new BookJPanel(userProcessContainer,customerDirectory,flightDirectory,ticketDirectory));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_bookBtnActionPerformed

    private void btnOrderHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderHistoryActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add(new OrderHistoryJPanel(userProcessContainer,customerDirectory,ticketDirectory));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnOrderHistoryActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JButton bookBtn;
    private javax.swing.JButton btnOrderHistory;
    private javax.swing.JButton mngCtmBtn;
    // End of variables declaration//GEN-END:variables
}
