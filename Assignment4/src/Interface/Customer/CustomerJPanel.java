/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Customer;

import Business.Order.Ticket;
import Business.Order.TicketDirectory;
import Business.People.Customer;
import Business.People.CustomerDirectory;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author xieli
 */
public class CustomerJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CustomerJPanel
     */
    JPanel userProcessContainer;
    CustomerDirectory customerDirectory;
    TicketDirectory ticketDirectory;
    String name;
    Customer customer;
    ArrayList<Ticket> tempList = new ArrayList<Ticket>();
    
    


    CustomerJPanel(JPanel userProcessContainer, CustomerDirectory customerDirectory,String name,TicketDirectory ticketDirectory) {
        initComponents();
        this.customerDirectory = customerDirectory;
        this.userProcessContainer = userProcessContainer;
        this.ticketDirectory = ticketDirectory;
        this.name = name;   
        //find the specific customer in login page
        for(int i = 0;i<customerDirectory.getCustomerList().size();i++){
            if(customerDirectory.getCustomerList().get(i).getUsername().equals(name)){
               
               customer = customerDirectory.getCustomerList().get(i);
               System.out.println(i+"!"+customer.getName());
            }
        }
        
        populate();
        
        
    }
    
    public void populate(){
        DefaultTableModel dtm = (DefaultTableModel)jTable1.getModel();
        dtm.setRowCount(0);
        for(int i=0;i<ticketDirectory.getTicketList().size();i++){
            System.out.println(customer);
            if(ticketDirectory.getTicketList().get(i).getCustomer().equals(customer)){
                tempList.add(ticketDirectory.getTicketList().get(i));
                System.out.println(i);
            }
        }
        

        for(Ticket t : tempList){
            Object[] row = new Object[dtm.getColumnCount()+1];
            row[0] = t;
            row[1] = t.getCustomer().getName();
            row[2] = t.getCustomer().getOffice();
            row[3] = t.getFleet().getFlightNum();
            row[4] = t.getSeat();
            row[5] = t.getFleet().getStatus();
            row[6] = t.isOrderStatus();
            row[7] = t.getFleet().getDate();
            dtm.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnOrder = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnOrder1 = new javax.swing.JButton();

        btnOrder.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnOrder.setText("View this order");
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });

        btnBack.setText("< -  Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order Number1", "Name", "Office", "Flight Number", "Seat", "Flight Status", "Order Status", "Date"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        btnOrder1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnOrder1.setText("Cancel this order");
        btnOrder1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrder1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(120, 120, 120)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOrder)
                    .addComponent(btnBack)
                    .addComponent(btnOrder1))
                .addGap(53, 53, 53)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1053, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(68, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(50, 50, 50)
                        .addComponent(btnOrder)
                        .addGap(49, 49, 49)
                        .addComponent(btnOrder1)))
                .addContainerGap(91, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        // TODO add your handling code here:
        int selectedRow= jTable1.getSelectedRow();
        if(selectedRow>=0){
            Ticket car= (Ticket) jTable1.getValueAt(selectedRow,0);
            MyOrderJPanel panel= new MyOrderJPanel(userProcessContainer,car);
            userProcessContainer.add("ViewCarJPanel",panel);
            CardLayout layout=(CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select a row from table first","warning",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnOrderActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnOrder1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrder1ActionPerformed
        // TODO add your handling code here:
        int selectedRow= jTable1.getSelectedRow();
        if(selectedRow>=0){
            Ticket car= (Ticket) jTable1.getValueAt(selectedRow,0);
            car.setOrderStatus(false);
            
            //find the seat
            if(car.getSeat()!=""){    
                if(car.getSeat().substring(0, 1).equals("A")){               
                    if(car.getSeat().substring(3).equals("Window")){
                        car.getFlight().seatList1.add(car.getSeat().substring(0, 2));
                    }else if(car.getSeat().substring(3).equals("Middle")){
                        car.getFlight().seatList2.add(car.getSeat().substring(0, 2));
                    }else{
                        car.getFlight().seatList3.add(car.getSeat().substring(0, 2));
                    }  
                }else{
                    if(car.getSeat().substring(3).equals("Window")){
                        car.getFlight().seatList6.add(car.getSeat().substring(0, 2));
                    }else if(car.getSeat().substring(3).equals("Middle")){
                        car.getFlight().seatList5.add(car.getSeat().substring(0, 2));
                    }else{
                        car.getFlight().seatList4.add(car.getSeat().substring(0, 2));
                    }
                }  
            }
    
            JOptionPane.showMessageDialog(null,"Successfully canceled.");
            populate();
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select a row from table first","warning",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnOrder1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnOrder1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
